let map = L.map('map').setView([-13.52264, -71.96734], 13);

L.tileLayer('http://{s}.tile.stamen.com/watercolor/{z}/{x}/{y}.jpg', {
    maxZoom: 18,
}).addTo(map);

L.marker([-13.52264, -71.96734]).addTo(map);

//	https://tiles.wmflabs.org/osm-no-labels/${z}/${x}/${y}.png
//http://{s}.tile.stamen.com/watercolor/{z}/{x}/{y}.jpg